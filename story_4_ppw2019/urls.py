"""story_4_ppw2019 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin

from webpage.views import index, contacts, projects, about, secret, activity_form, see_activity, save_schedule, clear_schedule

urlpatterns = [
    path('', index, name="index"),
    path('contacts/', contacts, name='contacts'),
    path('projects/', projects, name='projects'),
    path('about-me/', about, name='about'),
    path('secret/', secret, name='secret'),
    path('activity_form/', activity_form, name='activity_form'),
    path('see_activity/', see_activity, name='see_activity' ),
    path('activity_form/save_schedule/', save_schedule, name='save_schedule'),
    path('see_activity/<int:item_id>/', clear_schedule, name='clear_schedule')
]
