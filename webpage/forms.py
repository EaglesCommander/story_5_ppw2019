from django import forms
from .models import Schedule
from django.utils import timezone
from datetime import datetime, date

class Add_Schedule(forms.Form):

        date_attrs = {'type':'date','class':'form-control'}
        time_attrs = {'type':'time','class':'form-control'}
        activity_attrs = {'type':'activity','class':'form-control','placeholder':'Enter activity'}
        location_attrs = {'type':'location','class':'form-control','placeholder':'Enter location'}
        category_attrs = {'type':'category','class':'form-control', 'placeholder':'Enter category'}
        
        date = forms.DateTimeField(label='date', required=True, localize=True, widget=forms.DateInput(attrs=date_attrs))
        time = forms.TimeField(label='time', required=True, localize=True, widget=forms.TimeInput(attrs=time_attrs))
        activity = forms.CharField(label='activity', required=True, max_length=30, widget=forms.TextInput(attrs=activity_attrs))
        location = forms.CharField(label='location', required=True, max_length=30, widget=forms.TextInput(attrs=location_attrs))
        category = forms.CharField(label='category', required=True, max_length=30, widget=forms.TextInput(attrs=category_attrs))

class Clear_form(forms.Form):

        pass