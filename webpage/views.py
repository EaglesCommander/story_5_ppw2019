from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .forms import Add_Schedule, Clear_form
from .models import Schedule
from django.utils import timezone
from datetime import datetime, date

def index(request):
	return render (request, 'homepage.html')

def contacts(request):
    return render (request, 'contacts.html')

def about(request):
    return render (request, 'about_me.html')

def projects(request):
    return render (request, 'projects.html')

def secret(request):
    return render (request, 'secret.html')

def activity_form(request, response={"schedule": Add_Schedule}):
    return render (request, 'activity_form.html', response)

def see_activity(request):
    response = {"schedule": Schedule.objects.all().values}
    return render (request, 'see_activity.html', response)

def save_schedule(request):
    form = Add_Schedule(request.POST or None)

    if(request.method == 'POST' and form.is_valid()):
        date = form.cleaned_data['date'].strftime("%A %D")
        time = form.cleaned_data['time'].strftime("%H:%M %p")
        activity = form.cleaned_data['activity']
        location = form.cleaned_data['location']
        category = form.cleaned_data['category']

        schedule = Schedule(date=date, time=time, activity=activity, location=location, category=category)
        schedule.save()
    
    return HttpResponseRedirect('/activity_form/')

def clear_schedule(request, item_id=None):
    try:
        obj =  Schedule.objects.get(id=item_id)
        obj.delete()
    except:
        pass

    return HttpResponseRedirect('/see_activity/')