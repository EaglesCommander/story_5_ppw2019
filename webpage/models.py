from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.

class Schedule(models.Model):

    date = models.CharField(("Date"), max_length=30)
    time = models.CharField(("Time"), max_length=30)
    activity = models.CharField(("Activity"), max_length=30)
    location = models.CharField(("Location"), max_length=30)
    category = models.CharField(("Category"), max_length=30)