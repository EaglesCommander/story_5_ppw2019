# Generated by Django 2.2.5 on 2019-10-05 06:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField(verbose_name='Schedule')),
                ('time', models.TimeField(verbose_name='Time')),
                ('day', models.CharField(max_length=9, verbose_name='Day')),
                ('activity', models.CharField(max_length=255, verbose_name='Activity')),
                ('location', models.CharField(max_length=255, verbose_name='Location')),
                ('category', models.CharField(max_length=255, verbose_name='Category')),
            ],
        ),
    ]
